from typing import Annotated, Optional
from sqlalchemy.orm import Mapped, mapped_column, relationship
from .database import Base, str_256
from sqlalchemy import text, ForeignKey
import datetime


intpk = Annotated[int, mapped_column(primary_key=True)]
created_at = Annotated[datetime.datetime, mapped_column(server_default=text("TIMEZONE('utc', now())"))]
updated_at = Annotated[datetime.datetime, mapped_column(
        server_default=text("TIMEZONE('utc', now())"),
        onupdate=datetime.datetime.utcnow,
    )]


class PostsORM(Base):
    __tablename__ = "posts"

    id: Mapped[intpk]
    theme: Mapped[str_256]
    content: Mapped[str]
    count_like: Mapped[int] = mapped_column(default=0)
    author: Mapped[str_256]
    created_at: Mapped[created_at]
    updated_at: Mapped[updated_at]


