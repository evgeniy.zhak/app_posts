from .database import Base, async_engine
from .models import PostsORM
from sqlalchemy import select, delete, insert
from .shcemas import PostsDTO
from .database import async_session_factory
from .auth.models import role


class AsyncORM:
    @staticmethod
    async def create_tables():
        async with async_engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)

    @staticmethod
    async def new_post(theme, content, author, session):
        new_post = PostsORM(theme=theme, content=content, author=author)
        session.add(new_post)
        await session.commit()
        await session.refresh(new_post)
        result_dto = PostsDTO.model_validate(new_post, from_attributes=True)
        return result_dto

    @staticmethod
    async def all_posts(session):
        query = select(PostsORM)
        result = await session.execute(query)
        workers = result.scalars().all()
        result_dto = [PostsDTO.model_validate(row, from_attributes=True) for row in workers]
        return result_dto

    @staticmethod
    async def delete_post(post_id, session):
        query = delete(PostsORM).where(PostsORM.id == int(post_id))
        await session.execute(query)
        await session.commit()

    @staticmethod
    async def put_post(post_id, theme, content, author, session):
        post = await session.get(PostsORM, int(post_id))
        if post:
            post.theme = theme
            post.content = content
            post.author = author
            session.add(post)
            await session.commit()
            await session.refresh(post)
            result_dto = PostsDTO.model_validate(post, from_attributes=True)
            return result_dto
        else:
            return None

    @staticmethod
    async def like(post_id, session):
        post = await session.get(PostsORM, int(post_id))
        post.count_like += 1
        session.add(post)
        await session.commit()

    @staticmethod
    async def unlike(post_id, session):
        post = await session.get(PostsORM, int(post_id))
        post.count_like -= 1
        session.add(post)
        await session.commit()

    @staticmethod
    async def create_role():
        async with async_session_factory() as session:
            stmt = insert(role).values([
                {"name": "admin", "permissions": "admin"},
                {"name": "user", "permissions": "user"}
            ])

            await session.execute(stmt)
            await session.commit()
