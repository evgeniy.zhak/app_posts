import uvicorn
from fastapi import FastAPI, Request
import asyncio
from fastapi.middleware.cors import CORSMiddleware
from .orm import AsyncORM
from .router import router, router_like
from .pages import router_page
from .auth.schemas import UserRead, UserCreate
from .auth.base_config import fastapi_users,auth_backend


async def main():
    await AsyncORM.create_tables()
    await AsyncORM.create_role()


def create_fastapi_app():
    app = FastAPI(title="FastAPI")

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
    )
    app.include_router(router)
    app.include_router(router_like)
    app.include_router(router_page)

    app.include_router(
        fastapi_users.get_auth_router(auth_backend),
        prefix="/auth/jwt",
        tags=["auth"],
    )
    app.include_router(
        fastapi_users.get_register_router(UserRead, UserCreate),
        prefix="/auth",
        tags=["auth"],
    )

    return app


app = create_fastapi_app()


if __name__ == "__main__":
    asyncio.run(main())
    uvicorn.run(
        app="main:app",
        reload=True,
        host="0.0.0.0",
        port=8000,
    )

