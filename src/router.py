from fastapi import Response, Depends, APIRouter
from typing import Optional
from .orm import AsyncORM
from .shcemas import PostsAddDTO, PostsDTO
from typing import List
from .database import get_async_session


router = APIRouter(prefix='/api', tags=['posts'])
router_like = APIRouter(prefix='/api', tags=['posts/like'])


@router.post("/posts", response_model=PostsDTO)
async def create_new_posts(post_data: PostsAddDTO, session=Depends(get_async_session)):
    theme = post_data.theme
    content = post_data.content
    author = post_data.author
    res = await AsyncORM.new_post(theme=theme, content=content, author=author, session=session)
    return res


@router.get("/posts", response_model=List[PostsDTO])
async def get_all_posts(session=Depends(get_async_session)):
    res = await AsyncORM.all_posts(session=session)
    return res


@router.delete("/posts/<int:id>")
async def delete_posts(post_id, session=Depends(get_async_session)):
    await AsyncORM.delete_post(post_id, session=session)
    return 'Post was delete', 204


@router.put("/posts/<int:id>", response_model=Optional[PostsDTO])
async def put_posts(post_data: PostsAddDTO, post_id, session=Depends(get_async_session)):
    theme = post_data.theme
    content = post_data.content
    author = post_data.author
    res = await AsyncORM.put_post(post_id=post_id, theme=theme, content=content, author=author, session=session)
    if res:
        return res
    else:
        return Response(content="Object not found", status_code=404)


@router_like.patch("/posts/like/{post_id}")
async def patch_posts(post_id: int, session=Depends(get_async_session)):
    await AsyncORM.like(post_id, session=session)
    return f'like post {post_id}'


@router_like.patch("/posts/unlike/{post_id}")
async def patch_posts(post_id, session=Depends(get_async_session)):
    await AsyncORM.unlike(post_id, session=session)
    return f'unlike post {post_id}'
