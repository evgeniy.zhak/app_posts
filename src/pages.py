from fastapi import Request, Depends, APIRouter
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from .orm import AsyncORM
from .database import get_async_session


router_page = APIRouter(prefix='/pages', tags=['pages'])

templates = Jinja2Templates(directory='./src/templates')


@router_page.get("/posts", response_class=HTMLResponse)
async def show_posts(request: Request, session=Depends(get_async_session)):
    posts = await AsyncORM.all_posts(session=session)
    return templates.TemplateResponse("posts.html", {"request": request, "posts": posts})


@router_page.get("/posts/add")
async def public_post(request: Request):
    return templates.TemplateResponse("add_posts.html", {"request": request})



