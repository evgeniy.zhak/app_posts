from pydantic import BaseModel, ConfigDict
from datetime import datetime


class PostsAddDTO(BaseModel):
    theme: str
    content: str
    author: str


class PostsDTO(PostsAddDTO):
    id: int
    count_like: int
    created_at: datetime
    updated_at: datetime


